Deep Learning BoF, Debconf18
============================

This repo contains the slides presented during the BoF session.

Here are some useful citation links which are not added into slides:

1. https://lists.debian.org/debian-science/2018/06/msg00005.html
2. https://lwn.net/Articles/760142/
